# rhodonite

A Python script for cleaning up ASC files for use with Topas PXRD refinement software.

The program transforms data files with two or more data columns, representing angle 2 Theta and intensity (as floating point numbers), to files with exactly two columns. The resulting files can be read with Topas software.


## Requirements

- [Python 3](https://www.python.org/) interpreter
- `tkinter` support for GUI (make sure to install `tkinter` at Python installation)


## Usage

The program processes all files with extension `.asc` (case-insensitive). The input files are read, and the first two columns of data are extracted from them and parsed as floating point numbers. New data files with extension `.xy` are then written to the output directory.

### Procedure

1. Click the "Browse" button next to the "Source directory" field and select the directory containing the input files. The list of input files should be updated automatically. If any files with appropriate extension are found, they are displayed in the "Files to process" field.
1. The destination directory for output files is automatically set to the same path as the source directory. If another directory is desired, select it using the "Browse" button next to the "Destination directory" field.
1. Select postfix that will be applied to the names of the processed files. The postfix will be applied between the file name and the file extension. The postfix can be empty.
1. Check "Overwrite existing files," if you want the program to overwrite already existing files with the same path. If left unchecked, the program should never overwrite any files.
1. Click the button "Process Files" to process the files shown in the "Files to process" field.
1. Read the output log. Any errors will be logged there. Additional error information is written to the standard error output, that can be read if the program is ran from the command line.
