#! /usr/bin/env python

#
# rhodonite.py
#
# A script for cleaning up ASC files.
#

#
# Constants
#

PROGRAM_NAME = 'rhodonite'
PROGRAM_VERSION = '0.1.0'
PROGRAM_DATE = '2023-04-20'

#
# Helper functions
#

import sys

def eprint(*args, **kwargs):
    """Prints to STDERR."""
    print(*args, file=sys.stderr, **kwargs)

#
# File processing functionality - librhodonite
#

import os

def rho_scan_dir(path, extension=None):
    """Scans a directory and returns a list of file paths that can be processed.

    If directory reading fails, `None` is returned.
    Files are filtered based on their extension, if it's a string. Filtering
    is case-insensitive. Extension string should start with a dot.
    Only file names without absolute path are returned.
    """

    try:
        ld = os.listdir(path)
    except Exception as e:
        eprint(f"Error reading directory '{path}': {e}")
        return None


    # Get only files
    files = [entry for entry in ld if os.path.isfile(os.path.join(path, entry))]

    # Filter by extension
    if not isinstance(extension, str):
        files.sort()
        return files;

    ext_files = []
    for entry in files:
        name, ext = os.path.splitext(entry)
        if ext.lower() == extension.lower():
            ext_files.append(entry)

    ext_files.sort()
    return ext_files

def rho_read_file(path):
    """Reads a data file and returns its data as a list of tuples, or `None`
    in case of errors.

    Each list element is a tuple and represents a single line in the file.
    The lenght of the tuples is 2. The tuples contain the first two float
    numbers parsed from the line.
    """

    pairs = []
    bad_chars = '\ufeff'

    try:
        with open(path) as f:
            for line in f:
                split = line.split()
                if len(split) < 2:
                    eprint(f"Less than two data columns in file '{path}'.")
                    return None

                try:
                    c1 = float(split[0].strip(bad_chars))
                    c2 = float(split[1].strip(bad_chars))
                except ValueError as e:
                    eprint(f"Value parsing error: {e}")
                    return None

                pair = (c1, c2)
                pairs.append(pair)
    except Exception as e:
        eprint(f"Error reading file '{path}': {e}")
        return None

    return pairs

def rho_write_data_file(path, data):
    """Writes data in the form of list of tuples to a new file.
    Returns `True` on success and `False` otherwise.

    Does not check if the file already exists.
    Assumes that the list consists of tuples with the length of 2.
    """

    try:
        with open(path, 'w') as f:
            for line in data:
                f.write('{:15.5f} {:15.5f}\n'.format(line[0], line[1]))
    except Exception as e:
        eprint(f"Error while writing data file '{path}': {e}")
        return False

    return True

def rho_construct_file_path(dest_dir, file_name, postfix='', extension=''):
    """Constructs the path for a new data file by applying postfix before the
    file extension, replacing the extension, and joining the file name with
    the destination directory path.

    Postfix and extension replacement are not applied when they are an
    empty string.
    Extension must start with a dot, if provided.
    """

    name, ext = os.path.splitext(file_name)
    if len(extension) > 0:
        ext = extension
    if len(postfix) > 0:
        name += postfix
    file_name = name + ext

    return os.path.join(dest_dir, file_name)

#
# GUI
#

import tkinter as tk
from tkinter import *
from tkinter import ttk, filedialog

class Gui:
    """GUI window with all relevant data and methods."""

    def __init__(self):
        """Initializes a new GUI window."""

        # Important data

        # Path to the source directory
        self.source_dir = None
        # Path to the destination directory
        self.dest_dir = None
        # List of paths to the files to be processed
        self.file_list = []
        # Postfix to be applied to the names of processed files
        self.postfix = '_rho'
        # Overwrite existing files while processing
        self.overwrite = False

        # Window

        self.root = Tk();
        self.root.title('rhodonite')

        self.frm = ttk.Frame(self.root, padding=12)
        self.frm.grid()

        pad = {
            'padx': 5,
            'pady': 3
        }
        pad_plus = {
            'padx': pad['padx'],
            'pady': (pad['pady'] + 5, pad['pady'])
        }
        clr_path = '#F9F5EB'

        # Source dir interface
        ttk.Label(
                self.frm,
                text='Source directory:'
                ).grid(row=0, column=0, sticky='W')
        ttk.Button(
                self.frm,
                text='Browse',
                command = self.get_source_dir
                ).grid(row=0, column=1, sticky='E', **pad)
        self.label_source_dir = ttk.Label(
                self.frm,
                text='???',
                background=clr_path
                )
        self.label_source_dir.grid(row=1, column=0, columnspan=2,
                                   sticky='WE', **pad)

        # Source file list
        ttk.Label(
                self.frm,
                text='Files to process:'
                ).grid(row=2, column=0, columnspan=2, sticky='W', **pad_plus)
        self.list_items_source_files = tk.Variable(value=self.file_list)
        tk.Listbox(
                self.frm,
                listvariable=self.list_items_source_files,
                height=8
                ).grid(row=3, column=0, columnspan=2, sticky='WE', **pad)

        # Destination dir
        ttk.Label(
                self.frm,
                text='Destination directory:'
                ).grid(row=4, column=0, sticky='W')
        ttk.Button(
                self.frm,
                text='Browse',
                command = self.get_dest_dir
                ).grid(row=4, column=1, sticky='E', **pad)
        self.label_dest_dir = ttk.Label(
                self.frm,
                text='???',
                background=clr_path
                )
        self.label_dest_dir.grid(row=5, column=0, columnspan=2,
                                 sticky='WE', **pad)

        # File name postfix
        ttk.Label(
                self.frm,
                text='Name postfix:'
                ).grid(row=6, column=0, sticky='W', **pad_plus)
        self.entry_postfix = ttk.Entry(
                self.frm,
                )
        self.entry_postfix.grid(row=6, column=1, sticky='WE', **pad_plus)
        self.entry_postfix.insert(0, self.postfix)

        # File overwrite
        self.checkbutton_overwrite_var = tk.IntVar()
        ttk.Checkbutton(
                self.frm,
                text='Overwrite existing files',
                command=self.update_overwrite,
                variable=self.checkbutton_overwrite_var
                ).grid(row=7, column=0, columnspan=2, sticky='W', **pad)

        # Log
        ttk.Label(
                self.frm,
                text='Log:'
                ).grid(row=8, column=0, columnspan=2, sticky='W', **pad_plus)
        self.text_log = tk.Text(
                self.frm,
                height=12,
                width=60,
                state='disabled'
                )
        self.text_log.grid(row=9, column=0, columnspan=2, sticky='WE', **pad)

        # Control buttons
        ttk.Button(
                self.frm,
                text='Refresh input files',
                command=self.refresh_input_files
                ).grid(row=10, column=0, **pad)
        self.button_process = ttk.Button(
                self.frm,
                text='Process files',
                state='disabled',
                command=self.process_files
                )
        self.button_process.grid(row=10, column=1, **pad)

    def main_loop(self):
        """Enters the main loop of the GUI window."""
        self.root.mainloop()

    def get_directory(self, prompt=None) -> str:
        """Displays choose directory dialog and returns the resulting path."""
        return filedialog.askdirectory(
                parent=self.root,
                title=prompt,
                mustexist=True
                )

    def get_source_dir(self):
        """Gets source directory and updates the widget with its path."""
        path = self.get_directory('Select source directory')
        if path:
            self.label_source_dir['text'] = path
            self.source_dir = path
            self.label_dest_dir['text'] = path
            self.dest_dir = path
            self.refresh_input_files()

    def get_dest_dir(self):
        """Gets destination directory and updates the widget with its path."""
        path = self.get_directory('Select destinaton directory')
        if path:
            self.label_dest_dir['text'] = path
            self.dest_dir = path
    
    def log_push(self, message: str):
        """Appends a log message to the log widget."""
        if not message.endswith('\n'):
            message += '\n'

        self.text_log['state'] = 'normal'
        self.text_log.insert(tk.END, message)
        self.text_log['state'] = 'disabled'
        self.text_log.see(tk.END)

    def update_file_list(self):
        """Scans source directory for files to be processed, stores the
        list in this object's storage, and updates the list box with
        file names."""

        files = rho_scan_dir(self.source_dir, '.asc')
        if files is None:
            self.log_push(f"Scanning directory '{self.source_dir}' " +
                'for input files failed.')
        else:
            n = len(files)
            self.log_push(f"{n} input files found in directory " +
                f"'{self.source_dir}' ")
            self.file_list = files
            self.list_items_source_files.set(self.file_list)

    def update_button_state(self):
        """Enables or disables the process files button depending on
        whether all the required information is known."""

        if (
                len(self.file_list) > 0 and
                len(self.source_dir) > 0 and
                len(self.dest_dir) > 0
            ):
            self.button_process['state'] = 'enabled'
        else:
            self.button_process['state'] = 'disabled'

    def process_files(self):
        """Processes the input files and writes the output files."""
        n_proc = 0
        n_err = 0

        self.log_push('-' * 50)

        for name in self.file_list:
            src_path = os.path.join(self.source_dir, name)
            dest_path = rho_construct_file_path(
                    self.dest_dir,
                    name,
                    self.postfix,
                    '.xy'
                    )

            if False == self.overwrite and os.path.exists(dest_path):
                self.log_push(f"Path '{dest_path}' already exists. " +
                              'Will not overwrite.')
                continue

            data = rho_read_file(src_path)
            if data is None:
                n_err += 1
                self.log_push(f"Failed reading input file '{src_path}'.")
                continue

            res = rho_write_data_file(dest_path, data)
            if True == res:
                n_proc += 1
            else:
                n_err += 1
                self.log_push(f"Failed to write file '{dest_path}'.")

        if n_err > 0:
            self.log_push(f"{n_proc} files processed, {n_err} errors occured.")
        else:
            self.log_push(f"{n_proc} files processed.")

    def update_overwrite(self):
        """Updates the overwrite setting depending on the state of the
        overwrite check button."""
        if 0 == self.checkbutton_overwrite_var:
            self.overwrite = False
        else:
            self.overwrite = True

    def refresh_input_files(self):
        """Refreshes the list of available input files and sets button state."""
        if self.source_dir:
            self.update_file_list()
            self.update_button_state()

#
# MAIN
#

gui = Gui()
gui.log_push(f"{PROGRAM_NAME} version {PROGRAM_VERSION} " +
             f"({PROGRAM_DATE})")
gui.main_loop()
